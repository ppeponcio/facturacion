<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bill;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Numbers_Words;
/**
 * Bill controller.
 *
 * @Route("bill")
 */
class BillController extends Controller
{
    /**
     * Lists all bill entities.
     *
     * @Route("/", name="bill_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $bills = $em->getRepository('AppBundle:Bill')->findAll();

        return $this->render('bill/index.html.twig', array(
            'bills' => $bills,
        ));
    }

    /**
     * Creates a new bill entity.
     *
     * @Route("/new", name="bill_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $bill = new Bill();
        $form = $this->createForm('AppBundle\Form\BillType', $bill);
        $form->handleRequest($request);
        $items = $bill->getItems();
        $total=0;
        foreach ($items as $item){
            $item->setBill($bill);
            $item->setItemTotal($item->getItemValue()*$item->getItemQuantity());
            $total+=$item->getItemTotal();
        }
        $bill->setBillTotal($total);
        $bill->setBillTotalChar(Numbers_Words::toWords($total,'es'));

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bill);
            $em->flush();

            return $this->redirectToRoute('bill_show', array('id' => $bill->getId()));
        }

        return $this->render('bill/new.html.twig', array(
            'bill' => $bill,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a bill entity.
     *
     * @Route("/{id}", name="bill_show")
     * @Method("GET")
     */
    public function showAction(Bill $bill)
    {
        $deleteForm = $this->createDeleteForm($bill);

        return $this->render('bill/show.html.twig', array(
            'bill' => $bill,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing bill entity.
     *
     * @Route("/{id}/edit", name="bill_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Bill $bill)
    {
        $deleteForm = $this->createDeleteForm($bill);
        $editForm = $this->createForm('AppBundle\Form\BillType', $bill);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bill_edit', array('id' => $bill->getId()));
        }

        return $this->render('bill/edit.html.twig', array(
            'bill' => $bill,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a bill entity.
     *
     * @Route("/{id}", name="bill_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Bill $bill)
    {
        $form = $this->createDeleteForm($bill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bill);
            $em->flush();
        }

        return $this->redirectToRoute('bill_index');
    }

    /**
     * Creates a form to delete a bill entity.
     *
     * @param Bill $bill The bill entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bill $bill)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bill_delete', array('id' => $bill->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
