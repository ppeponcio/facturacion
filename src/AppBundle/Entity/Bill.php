<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Bill
 *
 * @ORM\Table(name="bill")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BillRepository")
 */
class Bill
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="billNumber", type="string", length=20)
     */
    private $billNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="billNotes", type="string", length=255)
     */
    private $billNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="billTotal", type="decimal", precision=8, scale=2)
     */
    private $billTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="billTotalChar", type="string", length=255)
     */
    private $billTotalChar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="billDate", type="datetime")
     */
    private $billDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="billAnull", type="boolean")
     */
    private $billAnull;

    /**
     * @var string
     *
     * @ORM\Column(name="billReason", type="string", length=255)
     */
    private $billReason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="billPaid", type="boolean")
     */
    private $billPaid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="billPayDate", type="datetime")
     */
    private $billPayDate;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="bill", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $items;

    /**
     *@ORM\ManyToOne(targetEntity="Client", inversedBy="bills")
     *@ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set billNumber.
     *
     * @param string $billNumber
     *
     * @return Bill
     */
    public function setBillNumber($billNumber)
    {
        $this->billNumber = $billNumber;

        return $this;
    }

    /**
     * Get billNumber.
     *
     * @return string
     */
    public function getBillNumber()
    {
        return $this->billNumber;
    }

    /**
     * Set billNotes.
     *
     * @param string $billNotes
     *
     * @return Bill
     */
    public function setBillNotes($billNotes)
    {
        $this->billNotes = $billNotes;

        return $this;
    }

    /**
     * Get billNotes.
     *
     * @return string
     */
    public function getBillNotes()
    {
        return $this->billNotes;
    }

    /**
     * Set billTotal.
     *
     * @param string $billTotal
     *
     * @return Bill
     */
    public function setBillTotal($billTotal)
    {
        $this->billTotal = $billTotal;

        return $this;
    }

    /**
     * Get billTotal.
     *
     * @return string
     */
    public function getBillTotal()
    {
        return $this->billTotal;
    }

    /**
     * Set billTotalChar.
     *
     * @param string $billTotalChar
     *
     * @return Bill
     */
    public function setBillTotalChar($billTotalChar)
    {
        $this->billTotalChar = $billTotalChar;

        return $this;
    }

    /**
     * Get billTotalChar.
     *
     * @return string
     */
    public function getBillTotalChar()
    {
        return $this->billTotalChar;
    }

    /**
     * Set billDate.
     *
     * @param \DateTime $billDate
     *
     * @return Bill
     */
    public function setBillDate($billDate)
    {
        $this->billDate = $billDate;

        return $this;
    }

    /**
     * Get billDate.
     *
     * @return \DateTime
     */
    public function getBillDate()
    {
        return $this->billDate;
    }

    /**
     * Set billAnull.
     *
     * @param bool $billAnull
     *
     * @return Bill
     */
    public function setBillAnull($billAnull)
    {
        $this->billAnull = $billAnull;

        return $this;
    }

    /**
     * Get billAnull.
     *
     * @return bool
     */
    public function getBillAnull()
    {
        return $this->billAnull;
    }

    /**
     * Set billReason.
     *
     * @param string $billReason
     *
     * @return Bill
     */
    public function setBillReason($billReason)
    {
        $this->billReason = $billReason;

        return $this;
    }

    /**
     * Get billReason.
     *
     * @return string
     */
    public function getBillReason()
    {
        return $this->billReason;
    }

    /**
     * Set billPaid.
     *
     * @param bool $billPaid
     *
     * @return Bill
     */
    public function setBillPaid($billPaid)
    {
        $this->billPaid = $billPaid;

        return $this;
    }

    /**
     * Get billPaid.
     *
     * @return bool
     */
    public function getBillPaid()
    {
        return $this->billPaid;
    }

    /**
     * Set billPayDate.
     *
     * @param \DateTime $billPayDate
     *
     * @return Bill
     */
    public function setBillPayDate($billPayDate)
    {
        $this->billPayDate = $billPayDate;

        return $this;
    }

    /**
     * Get billPayDate.
     *
     * @return \DateTime
     */
    public function getBillPayDate()
    {
        return $this->billPayDate;
    }

    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * set items
     *
     * @return \AppBundle\Entity\Item
     */
    public function setItems(\Doctrine\Common\Collections\ArrayCollection $items)
    {
        $this->items = $items;
        foreach ($items as $item) {
            $item->setBill($this);
        }
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return Bill
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
