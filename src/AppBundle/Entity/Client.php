<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clientFName", type="string", length=40)
     */
    private $clientFName;

    /**
     * @var string
     *
     * @ORM\Column(name="clientLName", type="string", length=255)
     */
    private $clientLName;

    /**
     * @var string
     *
     * @ORM\Column(name="clientAddress", type="string", length=20)
     */
    private $clientAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="clientPhone1", type="string", length=15)
     */
    private $clientPhone1;

    /**
     * @var string
     *
     * @ORM\Column(name="clientPhone2", type="string", length=15)
     */
    private $clientPhone2;

    /**
     * @var string
     *
     * @ORM\Column(name="clientFax", type="string", length=20)
     */
    private $clientFax;

    /**
     * @var string
     *
     * @ORM\Column(name="clientCity", type="string", length=20)
     */
    private $clientCity;

    /**
     * @var string
     *
     * @ORM\Column(name="clientDesc", type="string", length=255)
     */
    private $clientDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="clientDiscount", type="decimal", precision=3, scale=2)
     */
    private $clientDiscount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="clientCreation", type="datetime")
     */
    private $clientCreation;

    /**
     * @ORM\OneToMany(targetEntity="Bill", mappedBy="client")
     * @Assert\Valid()
     */
    private $bills;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientFName.
     *
     * @param string $clientFName
     *
     * @return Client
     */
    public function setClientFName($clientFName)
    {
        $this->clientFName = $clientFName;

        return $this;
    }

    /**
     * Get clientFName.
     *
     * @return string
     */
    public function getClientFName()
    {
        return $this->clientFName;
    }

    /**
     * Set clientLName.
     *
     * @param string $clientLName
     *
     * @return Client
     */
    public function setClientLName($clientLName)
    {
        $this->clientLName = $clientLName;

        return $this;
    }

    /**
     * Get clientLName.
     *
     * @return string
     */
    public function getClientLName()
    {
        return $this->clientLName;
    }

    /**
     * Set clientAddress.
     *
     * @param string $clientAddress
     *
     * @return Client
     */
    public function setClientAddress($clientAddress)
    {
        $this->clientAddress = $clientAddress;

        return $this;
    }

    /**
     * Get clientAddress.
     *
     * @return string
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * Set clientPhone1.
     *
     * @param string $clientPhone1
     *
     * @return Client
     */
    public function setClientPhone1($clientPhone1)
    {
        $this->clientPhone1 = $clientPhone1;

        return $this;
    }

    /**
     * Get clientPhone1.
     *
     * @return string
     */
    public function getClientPhone1()
    {
        return $this->clientPhone1;
    }

    /**
     * Set clientPhone2.
     *
     * @param string $clientPhone2
     *
     * @return Client
     */
    public function setClientPhone2($clientPhone2)
    {
        $this->clientPhone2 = $clientPhone2;

        return $this;
    }

    /**
     * Get clientPhone2.
     *
     * @return string
     */
    public function getClientPhone2()
    {
        return $this->clientPhone2;
    }

    /**
     * Set clientFax.
     *
     * @param string $clientFax
     *
     * @return Client
     */
    public function setClientFax($clientFax)
    {
        $this->clientFax = $clientFax;

        return $this;
    }

    /**
     * Get clientFax.
     *
     * @return string
     */
    public function getClientFax()
    {
        return $this->clientFax;
    }

    /**
     * Set clientCity.
     *
     * @param string $clientCity
     *
     * @return Client
     */
    public function setClientCity($clientCity)
    {
        $this->clientCity = $clientCity;

        return $this;
    }

    /**
     * Get clientCity.
     *
     * @return string
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * Set clientDesc.
     *
     * @param string $clientDesc
     *
     * @return Client
     */
    public function setClientDesc($clientDesc)
    {
        $this->clientDesc = $clientDesc;

        return $this;
    }

    /**
     * Get clientDesc.
     *
     * @return string
     */
    public function getClientDesc()
    {
        return $this->clientDesc;
    }

    /**
     * Set clientDiscount.
     *
     * @param string $clientDiscount
     *
     * @return Client
     */
    public function setClientDiscount($clientDiscount)
    {
        $this->clientDiscount = $clientDiscount;

        return $this;
    }

    /**
     * Get clientDiscount.
     *
     * @return string
     */
    public function getClientDiscount()
    {
        return $this->clientDiscount;
    }

    /**
     * Set clientCreation.
     *
     * @param \DateTime $clientCreation
     *
     * @return Client
     */
    public function setClientCreation($clientCreation)
    {
        $this->clientCreation = $clientCreation;

        return $this;
    }

    /**
     * Get clientCreation.
     *
     * @return \DateTime
     */
    public function getClientCreation()
    {
        return $this->clientCreation;
    }

    public function addBill(\AppBundle\Entity\Bill $bills)
    {
        $this->bills[] = $bills;

        return $this;
    }

    /**
     * Remove bills
     *
     * @param \AppBundle\Entity\Bill $bills
     */
    public function removeBill(\AppBundle\Entity\Bill $bills)
    {
        $this->bills->removeElement($bills);
    }

    /**
     * Get bills
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBills()
    {
        return $this->bills;
    }
    public function __toString()
    {
        return $this->clientFName.' '.$this->clientLName;
        return $this->clientFName.' '.$this->clientLName;
    }
}
