<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemRepository")
 */
class Item
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="itemQuantity", type="integer")
     */
    private $itemQuantity;

    /**
     * @var string
     *
     * @ORM\Column(name="itemDesc", type="string", length=255)
     */
    private $itemDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="itemValue", type="decimal", precision=8, scale=2)
     */
    private $itemValue;

    /**
     * @var string
     *
     * @ORM\Column(name="itemTotal", type="decimal", precision=8, scale=2)
     */
    private $itemTotal;

    /**
     *@ORM\ManyToOne(targetEntity="Bill", inversedBy="items")
     *@ORM\JoinColumn(name="bill_id", referencedColumnName="id")
     */
    protected $bill;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemQuantity.
     *
     * @param int $itemQuantity
     *
     * @return Item
     */
    public function setItemQuantity($itemQuantity)
    {
        $this->itemQuantity = $itemQuantity;

        return $this;
    }

    /**
     * Get itemQuantity.
     *
     * @return int
     */
    public function getItemQuantity()
    {
        return $this->itemQuantity;
    }

    /**
     * Set itemDesc.
     *
     * @param string $itemDesc
     *
     * @return Item
     */
    public function setItemDesc($itemDesc)
    {
        $this->itemDesc = $itemDesc;

        return $this;
    }

    /**
     * Get itemDesc.
     *
     * @return string
     */
    public function getItemDesc()
    {
        return $this->itemDesc;
    }

    /**
     * Set itemValue.
     *
     * @param string $itemValue
     *
     * @return Item
     */
    public function setItemValue($itemValue)
    {
        $this->itemValue = $itemValue;

        return $this;
    }

    /**
     * Get itemValue.
     *
     * @return string
     */
    public function getItemValue()
    {
        return $this->itemValue;
    }

    /**
     * Set itemTotal.
     *
     * @param string $itemTotal
     *
     * @return Item
     */
    public function setItemTotal($itemTotal)
    {
        $this->itemTotal = $itemTotal;

        return $this;
    }

    /**
     * Get itemTotal.
     *
     * @return string
     */
    public function getItemTotal()
    {
        return $this->itemTotal;
    }

    /**
     * Set bill
     *
     * @param Bill $bill
     * @return Item
     */
    public function setBill(Bill $bill = null)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get bill
     *
     * @return Bill
     */
    public function getBill()
    {
        return $this->bill;
    }
}
